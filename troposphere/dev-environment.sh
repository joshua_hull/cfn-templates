#! /bin/bash

# Install Docker apt GPG key
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -

# Add Docker apt repo
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

# Update and install docker
apt-get update
apt-get install -y docker-ce

# Create 'coder' user and group
adduser --disable-password --gecos "" coder

# Run code-server
docker run -it -p 8080:8080 -u ${id -u coder}:${id -g coder} -v "/home/coder/.local/share/code-server:/home/coder/.local/share/code-server" -v "/home/coder/project:/home/coder/project" codercom/code-server:v2 --auth none