from troposphere import (
    Base64,
    Cidr,
    FindInMap,
    GetAtt,
    Join,
    Ref,
    Select,
    Tags,
    Template
)

from troposphere.ec2 import (
    EIP,
    InternetGateway,
    NatGateway,
    Route,
    RouteTable,
    SecurityGroup,
    SecurityGroupRule,
    Subnet,
    SubnetRouteTableAssociation,
    VPC,
    VPCGatewayAttachment
)

from troposphere.autoscaling import (
    AutoScalingGroup,
    BlockDeviceMapping,
    EBSBlockDevice,
    LaunchConfiguration,
    Metadata
)

from troposphere.cloudformation import (
    Init,
    InitConfig,
    InitConfigSets,
    InitFile,
    InitFiles,
    InitService,
    InitServices
)

from troposphere.ecs import (
    Cluster
)

from troposphere.iam import (
    InstanceProfile,
    PolicyType,
    Role
)

from troposphere.policies import (
    AutoScalingReplacingUpdate,
    AutoScalingRollingUpdate,
    UpdatePolicy
)

from awacs.aws import (
    Allow
)

from awacs.aws import (
    Policy,
    Principal,
    Statement
)

from awacs.sts import (
    AssumeRole
)

import awacs.ecr as ecr
import awacs.ecs as ecs
import awacs.logs as logs

t = Template()

t.add_version("2010-09-09")

t.add_description("""\
feature/ecs-cluster\
""")

t.add_metadata({
    "cfn-lint": {
        "config": {
            "ignore_checks": ["E1024", "E3012"]
        }
    }
})

t.add_mapping("335981790998", {
    "us-east-1": {
        "AmiId": "ami-00129b193dc81bc31",
        "InstanceType": "t3.small",
        "ScalingGroupCapacity": "0",
        "SSHKey": "default",
        "VpcCIDR": "172.30.0.0/16"
    }
})

VPC = t.add_resource(VPC(
    "VPC",
    CidrBlock=FindInMap(Ref("AWS::AccountId"), Ref("AWS::Region"), "VpcCIDR")
))

INTERNET_GATEWAY = t.add_resource(InternetGateway("InternetGateway"))

VPC_GATEWAY_ATTACHMENT = t.add_resource(VPCGatewayAttachment(
    "VPCGatewayAttachment",
    InternetGatewayId=Ref(INTERNET_GATEWAY),
    VpcId=Ref(VPC)
))

PRIVATE_ROUTE_TABLE = t.add_resource(RouteTable(
    "PrivateRouteTable",
    VpcId=Ref(VPC)
))

NAT_EIP = t.add_resource(EIP(
    "NatEip",
    Domain="vpc",
    DependsOn=VPC_GATEWAY_ATTACHMENT
))

PUBLIC_ROUTE_TABLE = t.add_resource(RouteTable(
    "PublicRouteTable",
    VpcId=Ref(VPC)
))

DEFAULT_PUBLIC_ROUTE = t.add_resource(Route(
    "PublicDefaultRoute",
    DestinationCidrBlock="0.0.0.0/0",
    GatewayId=Ref(INTERNET_GATEWAY),
    RouteTableId=Ref(PUBLIC_ROUTE_TABLE)
))

subnetAZs = ["A", "B", "C"]
privateSubnets = []
for index, subnetAZ in enumerate(subnetAZs):
    SUBNET = t.add_resource(Subnet(
        "PrivateSubnet{}".format(subnetAZ),
        AvailabilityZone=Join("", [Ref("AWS::Region"), subnetAZ.lower()]),
        CidrBlock=Select(index + (len(subnetAZs) * 1), Cidr(FindInMap(Ref("AWS::AccountId"), Ref("AWS::Region"), "VpcCIDR"), len(subnetAZs) * 2, 8)),
        Tags=Tags(Name="Private Subnet {}".format(subnetAZ)),
        VpcId=Ref(VPC)
    ))

    PRIVATE_ROUTE_TABLE_ASSOCIATION = t.add_resource(SubnetRouteTableAssociation(
        "PrivateRouteTableAssociation{}".format(subnetAZ),
        SubnetId=Ref(SUBNET),
        RouteTableId=Ref(PRIVATE_ROUTE_TABLE)
    ))

    privateSubnets.append(SUBNET)

publicSubnets = []
for index, subnetAZ in enumerate(subnetAZs):
    SUBNET = t.add_resource(Subnet(
        "PublicSubnet{}".format(subnetAZ),
        AvailabilityZone=Join("", [Ref("AWS::Region"), subnetAZ.lower()]),
        CidrBlock=Select(index + (len(subnetAZs) * 2), Cidr(FindInMap(Ref("AWS::AccountId"), Ref("AWS::Region"), "VpcCIDR"), len(subnetAZs) * 3, 8)),
        MapPublicIpOnLaunch=True,
        Tags=Tags(Name="Public Subnet {}".format(subnetAZ)),
        VpcId=Ref(VPC)
    ))

    PUBLIC_ROUTE_TABLE_ASSOCIATION = t.add_resource(SubnetRouteTableAssociation(
        "PublicRouteTableAssociation{}".format(subnetAZ),
        SubnetId=Ref(SUBNET),
        RouteTableId=Ref(PUBLIC_ROUTE_TABLE)
    ))

    publicSubnets.append(SUBNET)

NAT = t.add_resource(NatGateway(
    "Nat",
    AllocationId=GetAtt(NAT_EIP, "AllocationId"),
    SubnetId=Ref(publicSubnets[0]),
))

t.add_resource(Route(
    "NatRoute",
    DestinationCidrBlock="0.0.0.0/0",
    NatGatewayId=Ref(NAT),
    RouteTableId=Ref(PRIVATE_ROUTE_TABLE)
))

ECS_CLUSTER = t.add_resource(Cluster(
    "ECSCluster",
    ClusterName="default"
))

CLUSTER_ROLE_POLICY_DOCUMENT = Policy(
    Statement=[
        Statement(
            Effect=Allow,
            Action=[AssumeRole],
            Principal=Principal("Service", ["ec2.amazonaws.com"])
        )
    ]
)

CLUSTER_ROLE = t.add_resource(Role(
    "ECSClusterRole",
    Path="/",
    AssumeRolePolicyDocument=CLUSTER_ROLE_POLICY_DOCUMENT
))

ECR_POLICY_DOCUMENT = Policy(
    Statement=[
        Statement(
            Effect=Allow,
            Action=[
                ecr.GetAuthorizationToken,
                ecr.BatchCheckLayerAvailability,
                ecr.GetDownloadUrlForLayer,
                ecr.GetRepositoryPolicy,
                ecr.DescribeRepositories,
                ecr.ListImages,
                ecr.BatchGetImage,
            ],
            Resource=["*"]
        )
    ]
)

ECR_POLICY = t.add_resource(PolicyType(
    "ECRPolicy",
    PolicyName="ECRPolicy",
    PolicyDocument=ECR_POLICY_DOCUMENT,
    Roles=[Ref(CLUSTER_ROLE)],
))

ECS_POLICY_DOCUMENT = Policy(
    Statement=[
        Statement(
            Effect=Allow,
            Action=[ecs.CreateCluster,
                    ecs.DeregisterContainerInstance,
                    ecs.DiscoverPollEndpoint,
                    ecs.Poll,
                    ecs.RegisterContainerInstance,
                    ecs.StartTelemetrySession,
                    ecr.GetAuthorizationToken,
                    ecr.BatchCheckLayerAvailability,
                    ecr.GetDownloadUrlForLayer,
                    ecr.BatchGetImage,
                    logs.CreateLogStream,
                    logs.PutLogEvents,
                    ecs.Action("Submit*")],
            Resource=["*"]
        )
    ]
)

ECS_POLICY = t.add_resource(PolicyType(
    "ECSPolicy",
    PolicyName="ECSPolicy",
    PolicyDocument=ECS_POLICY_DOCUMENT,
    Roles=[Ref(CLUSTER_ROLE)],
))


INSTANCE_PROFILE = t.add_resource(InstanceProfile(
    "InstanceProfile",
    Path="/",
    Roles=[Ref(CLUSTER_ROLE)],
))


LAUNCH_CONFIGURATION_NAME = "LaunchConfiguration"

LAUNCH_CONFIGURATION_METADATA=Metadata(Init(
    InitConfigSets(
        default=["config"]
    ),
    config=InitConfig(
        files=InitFiles({
            "/etc/ecs/ecs.config": InitFile(
                content=Join("", ["ECS_CLUSTER=", Ref(ECS_CLUSTER)]),
                mode="000644",
                owner="root",
                group="root"
            ),
            "/etc/cfn/cfn-hup.conf": InitFile(
                content=Join("", ["[main]\n", "stack=", Ref("AWS::StackName"), "\n", "region=", Ref("AWS::Region"), "\n" ]),
                mode="000400",
                owner="root",
                group="root"
            ),
            "/etc/cfn/hooks.d/cfn-auto-reloader.conf": InitFile(
                content=Join("", [
                    "[cfn-auto-reloader-hook]\n",
                    "triggers=post.update\n",
                    "path=Resources.{}.Metadata.AWS::CloudFormation::Init\n".format(LAUNCH_CONFIGURATION_NAME),
                    "action=/opt/aws/bin/cfn-init -v ",
                    "         --stack ", Ref("AWS::StackName"),
                    "         --resource {} ".format(LAUNCH_CONFIGURATION_NAME),
                    "         --configsets default ",
                    "         --region ", Ref("AWS::Region"), "\n",
                    "runas=root\n"
                ])
            )
        }),
        services={
            "sysvinit": InitServices({
                "cfn-hup": InitService(
                    enabled=True,
                    ensureRunning=True,
                    files=["/etc/cfn/cfn-hup.conf", "/etc/cfn/hooks.d/cfn-auto-reloader.conf"]
                )
            })
        },
        packages={
            "yum": {
                "aws-cfn-bootstrap": []
            }
        }
    )
))

LAUNCH_CONFIGURATION = t.add_resource(LaunchConfiguration(
    LAUNCH_CONFIGURATION_NAME,
    AssociatePublicIpAddress=False,
    EbsOptimized=True,
    ImageId=FindInMap(Ref("AWS::AccountId"), Ref("AWS::Region"), "AmiId"),
    InstanceMonitoring=False,
    InstanceType=FindInMap(Ref("AWS::AccountId"), Ref("AWS::Region"), "InstanceType"),
    BlockDeviceMappings=([
        BlockDeviceMapping(DeviceName="/dev/sda1", Ebs=EBSBlockDevice(VolumeSize=8, DeleteOnTermination=True)),
        BlockDeviceMapping(DeviceName="/dev/xvdcz", Ebs=EBSBlockDevice(VolumeSize=32, DeleteOnTermination=True))
    ]),
    Metadata=LAUNCH_CONFIGURATION_METADATA,
    IamInstanceProfile=Ref(INSTANCE_PROFILE),
    UserData=Base64(
        Join("",[
            "/opt/aws/bin/cfn-init ",
            "         --stack ", Ref("AWS::StackName"),
            "         --resource {} ".format(LAUNCH_CONFIGURATION_NAME),
            "         --configsets default ",
            "         --region ", Ref("AWS::Region"), "\n",
            "/opt/aws/bin/cfn-signal -e $?",
            "         --resource {} ".format(LAUNCH_CONFIGURATION_NAME),
            "         --stack ", Ref("AWS::StackName"),
            "         --region ", Ref("AWS::Region"), "\n"
        ])
    )
))

PRIVATE_SUBNET_REFS = []
for privateSubnet in privateSubnets:
    PRIVATE_SUBNET_REFS.append(Ref(privateSubnet))

AVAILABILITY_ZONES = []
for az in subnetAZs:
    AVAILABILITY_ZONES.append(Join("", [Ref("AWS::Region"), az.lower()]))

AUTOSCALING_GROUP = t.add_resource(AutoScalingGroup(
    "AutoScalingGroup",
    AvailabilityZones=AVAILABILITY_ZONES,
    DesiredCapacity=FindInMap(Ref("AWS::AccountId"), Ref("AWS::Region"), "ScalingGroupCapacity"),
    LaunchConfigurationName=Ref(LAUNCH_CONFIGURATION),
    MaxSize=FindInMap(Ref("AWS::AccountId"), Ref("AWS::Region"), "ScalingGroupCapacity"),
    MinSize=FindInMap(Ref("AWS::AccountId"), Ref("AWS::Region"), "ScalingGroupCapacity"),
    VPCZoneIdentifier=PRIVATE_SUBNET_REFS,
    UpdatePolicy=UpdatePolicy(
        AutoScalingReplacingUpdate=AutoScalingReplacingUpdate(
            WillReplace=True,
        ),
        AutoScalingRollingUpdate=AutoScalingRollingUpdate(
            WaitOnResourceSignals=True,
            MinInstancesInService=0
        )
    )
))

print(t.to_yaml())