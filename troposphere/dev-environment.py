import os

from troposphere import Template, Parameter, Ref, Join, GetAtt
import troposphere.ec2 as ec2
import troposphere.elasticloadbalancingv2 as elb
import troposphere.route53 as route53
from troposphere.helpers import userdata

instance_type = Parameter(
    "instancetype", 
    Description="Instance type to launch", 
    Type="String",
    Default="t3.medium"
)

subnet = Parameter(
    "subnet",
    Description="Subnet to launch into",
    Type = "List<AWS::EC2::Subnet::Id>"
)

vpc = Parameter(
    "vpc",
    Description="VPC to launch into",
    Type = "AWS::EC2::VPC::Id"
)

certificate = Parameter(
    "certificate",
    Description="Certificate ARN to use with ALB",
    Type = "String",
    Default = ""
)

domain = Parameter(
    "domain",
    Description="Domain to use with ALB",
    Type = "AWS::Route53::HostedZone::Id"
)

subdomain = Parameter(
    "subdomain",
    Description="Subdomain to use with ALB",
    Type = "String"
)

clientid = Parameter(
    "clientid",
    Description="Client ID to authenticate on ALB with GitLab",
    Type = "String",
    NoEcho=True
)

clientsecret = Parameter(
    "clientsecret",
    Description="Client Secret to authenticate on the ALB with GitLab",
    Type = "String",
    NoEcho=True
)

load_balancer_security_group = ec2.SecurityGroup(
    "loadbalancersecuritygroup",
    GroupDescription="Allow from Internet",
    SecurityGroupIngress=[
        ec2.SecurityGroupRule(
            IpProtocol="tcp",
            FromPort="80",
            ToPort="80",
            CidrIp="0.0.0.0/0"
        ),
        ec2.SecurityGroupRule(
            IpProtocol="tcp",
            FromPort="443",
            ToPort="443",
            CidrIp="0.0.0.0/0"
        )
    ]
)

instance_security_group = ec2.SecurityGroup(
    "instancesecuritygroup",
    GroupDescription="Allow from ALB",
    SecurityGroupIngress=[
        ec2.SecurityGroupRule(
            IpProtocol="tcp",
            FromPort="8080",
            ToPort="8080",
            SourceSecurityGroupId=Ref(load_balancer_security_group)
        )
    ]
)

instance = ec2.Instance(
    "codeserver",
    ImageId="ami-07ebfd5b3428b6f4d",
    InstanceType=Ref(instance_type),
    SecurityGroups=[Ref(instance_security_group)],
    UserData=userdata.from_file(os.path.join(os.path.dirname(__file__), "dev-environment.sh"))
)

load_balancer = elb.LoadBalancer(
    "loadbalancer",
    Subnets=Ref(subnet)
)

target_group = elb.TargetGroup(
    "targetgroup",
    HealthCheckIntervalSeconds="30",
    HealthCheckProtocol="HTTP",
    HealthCheckTimeoutSeconds="10",
    HealthyThresholdCount="2",
    Matcher=elb.Matcher(
        HttpCode="200-399"
    ),
    Protocol="HTTP",
    Port="8080",
    Targets=[
        elb.TargetDescription(
            Id=Ref(instance),
            Port="8080"
        )
    ],
    UnhealthyThresholdCount="2",
    VpcId=Ref(vpc)
)

listener = elb.Listener(
    "loadbalancerlistener",
    Port="80",
    Protocol="HTTP",
    LoadBalancerArn=Ref(load_balancer),
    DefaultActions=[
        elb.Action(
            Type="redirect",
            RedirectConfig=elb.RedirectConfig(
              Host="#{host}",
              Path="/#{path}",
              Port="443",
              Protocol="https",
              Query="#{query}",
              StatusCode="HTTP_302"  
            )
        )
    ]
)

listener_secure = elb.Listener(
    "secureloadbalancerlistener",
    Port="443",
    Protocol="HTTPS",
    LoadBalancerArn=Ref(load_balancer),
    DefaultActions=[
        elb.Action(
            Type="authenticate-oidc",
            AuthenticateOidcConfig=elb.AuthenticateOidcConfig(
                Issuer="https://gitlab.com",
                AuthorizationEndpoint="https://gitlab.com/oauth/authorize",
                TokenEndpoint="https://gitlab.com/oauth/token",
                UserInfoEndpoint="https://gitlab.com/oauth/userinfo",
                ClientId=Ref(clientid),
                ClientSecret=Ref(clientsecret),
                Scope="openid",
                OnUnauthenticatedRequest="deny"
            )       
        ),
        elb.Action(
            Type="forward",
            TargetGroupArn=Ref(instance)
        )
    ]
)

listener_secure_certificate = elb.ListenerCertificate(
    "listenercertificate",
    Certificates=[Ref(certificate)],
    ListenerArn=Ref(listener_secure)
)

dns = route53.RecordSetType(
    'dns',
    HostedZoneId=Ref(domain),
    Name=Join("", [Ref(subdomain), ".", Ref(domain)]),
    Type="A",
    AliasTarget=route53.AliasTarget(
        HostedZoneId=GetAtt(load_balancer, "CanonicalHostedZoneNameID"),
        DNSName=GetAtt(load_balancer, "DNSName")
    )
)

t = Template()

t.add_version("2010-09-09")

t.add_metadata({
    "cfn-lint": {
        "config": {
            "ignore_checks": ["E1024", "E3012"]
        }
    }
})

t.add_parameter(instance_type)
t.add_parameter(subnet)
t.add_parameter(vpc)
t.add_parameter(certificate)
t.add_parameter(domain)
t.add_parameter(subdomain)

t.add_resource(instance)
t.add_resource(load_balancer)
t.add_resource(load_balancer_security_group)
t.add_resource(instance_security_group)
t.add_resource(target_group)
t.add_resource(listener)
t.add_resource(listener_secure)
t.add_resource(listener_secure_certificate)
t.add_resource(dns)

print(t.to_yaml())