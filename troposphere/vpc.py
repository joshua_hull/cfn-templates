from troposphere import Template, Parameter, Ref, Cidr, Select
from troposphere.ec2 import VPC, Subnet, NetworkAcl, Route

t = Template()

t.add_version("2010-09-09")

t.add_description("""\
Simple Troposphere VPC Example.\
""")

VPC_CIDR = t.add_parameter(Parameter(
    "VPCCIDR",
    ConstraintDescription=(
        "Must be valid CIDR Range in the form of XXX.XXX.XXX.XXX/XX"
    ),
    Description="IP Address range for VPC",
    AllowedPattern=r"(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})/(\d{1,2})",
    Type="String"
))

SUBNET_COUNT = t.add_parameter(Parameter(
    "SUBNETCOUNT",
    Description="Subnet Count",
    ConstraintDescription=(
        "Must be between 1 and 32 inclusive"
    ),
    Type="Number",
    Default=1,
    MinValue=1,
    MaxValue=32
))

SUBNET_SIZE = t.add_parameter(Parameter(
    "SUBNETSIZE",
    Description="Subnet Mask Size",
    Type="Number",
     ConstraintDescription=(
        "Must be between 1 and 32 inclusive"
    ),
    MinValue=1,
    MaxValue=32
))

VPC = t.add_resource(VPC(
    "VPC",
    CidrBlock=Ref("VPCCIDR")
))

subnet = t.add_resource(
    Subnet(
        'Subnet',
        CidrBlock=Select(0, Cidr(Ref(VPC_CIDR), Ref(SUBNET_COUNT), Ref(SUBNET_SIZE))),
        VpcId=Ref(VPC)
        ))

print(t.to_yaml())